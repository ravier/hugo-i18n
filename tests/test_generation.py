# SPDX-FileCopyrightText: 2021 Nguyen Hung Phu <phuhnguyen@disroot.org>
# SPDX-License-Identifier: LGPL-2.1-or-later

import importlib.resources as pkg_resources
import os
import shutil
import unittest

import frontmatter

from hugoi18n import generation


class GenerationTestCase(unittest.TestCase):
    package = 'websites-kde-org'
    os.environ['PACKAGE'] = package

    def test_generate_content(self):
        with pkg_resources.open_text('tests.resources', 'distros.md') as f_dist:
            page = frontmatter.load(f_dist)
        locale_path = 'locale/vi/LC_MESSAGES'
        os.makedirs(locale_path, exist_ok=True)
        with pkg_resources.path('tests.resources', 'distros.mo') as p_distros:
            shutil.copyfile(p_distros, f'{locale_path}/distros.mo')
        os.environ["LANGUAGE"] = 'vi'
        _ = generation.gettext_func('distros')
        gen_content, pct = generation.generate_content(page.content, _)
        self.assertEqual(1, pct)

    def test_generate_blockquotes(self):
        with pkg_resources.open_text('tests.resources', 'blockquotes.md') as f_bq:
            page = frontmatter.load(f_bq)
        locale_path = 'locale/vi/LC_MESSAGES'
        os.makedirs(locale_path, exist_ok=True)
        with pkg_resources.path('tests.resources', 'blockquotes.mo') as p_bq:
            shutil.copyfile(p_bq, f'{locale_path}/blockquotes.mo')
        os.environ["LANGUAGE"] = 'vi'
        _ = generation.gettext_func('blockquotes')
        gen_content, pct = generation.generate_content(page.content, _)
        self.assertEqual(1, pct)

    def test_generate_codeblock(self):
        with pkg_resources.open_text('tests.resources', 'codeblock.md') as f_bq:
            page = frontmatter.load(f_bq)
        locale_path = 'locale/vi/LC_MESSAGES'
        os.makedirs(locale_path, exist_ok=True)
        with pkg_resources.path('tests.resources', 'codeblock.mo') as p_bq:
            shutil.copyfile(p_bq, f'{locale_path}/codeblock.mo')
        os.environ["LANGUAGE"] = 'vi'
        _ = generation.gettext_func('codeblock')
        gen_content, pct = generation.generate_content(page.content, _)
        print(gen_content)
        self.assertEqual(1, pct)


if __name__ == '__main__':
    unittest.main()
