# SPDX-FileCopyrightText: 2021 Nguyen Hung Phu <phuhnguyen@disroot.org>
# SPDX-License-Identifier: LGPL-2.1-or-later

import importlib.resources as pkg_resources
import os
import unittest

import frontmatter

from hugoi18n import extraction


class ExtractionTestCase(unittest.TestCase):
    package = 'websites-kde-org'
    os.environ['PACKAGE'] = package

    def test_extract_content(self):
        with pkg_resources.open_text('tests.resources', 'distros.md') as f_dist:
            page = frontmatter.load(f_dist)
        pot = extraction.create_pot(self.package)
        extraction.extract_content(page.content, pot, 'content/distros.md')
        self.assertEqual(7, len(pot))

    def test_extract_blockquotes(self):
        with pkg_resources.open_text('tests.resources', 'blockquotes.md') as f_bq:
            page = frontmatter.load(f_bq)
        pot = extraction.create_pot(self.package)
        extraction.extract_content(page.content, pot, 'content/blockquotes.md')
        self.assertEqual(12, len(pot))

    def test_extract_codeblock(self):
        with pkg_resources.open_text('tests.resources', 'codeblock.md') as f_bq:
            page = frontmatter.load(f_bq)
        pot = extraction.create_pot(self.package)
        extraction.extract_content(page.content, pot, 'content/codeblock.md')
        self.assertEqual(6, len(pot))


if __name__ == '__main__':
    unittest.main()
