---
title: A beautiful title
sassFiles:
- /sass/mine.sass
description: A magnificent description
layout: distro
distros:
  - name: Fire
    logo: /content/distros/logos/fire.svg
    image: /content/distros/bg/fire.png
    color: fire
    link: https://example.org/fire/
    description: Fire is a great distro
  - name: Magma
    logo: /content/distros/logos/magma.svg
    image: /content/distros/bg/magma.png
    color: magma
    link: https://example.org/magma/
    description: Magma is a nice distro
subtitle: This page lists some wonderful distros
authors:
  - SPDX-FileCopyrightText: 2021 Nguyen Hung Phu <phuhnguyen@disroot.org>
SPDX-License-Identifier: CC0-1.0
---

Those distros are nice!

+ [List item 1](http://example.org/item_1.zip)
+ [Item 2](http://example.org/item_2/)
+ [List item 3](http://example.org/third_item.tar.gz)
+ [List item 4](http://example.org/fourth_item.dmg)
+ {{% i18n_va "Playing %[1]s" "4.14.3" %}}

Another good sentence.

The last sentence. This should be realllllly longggg. Let's write some meaningless stuff here so it can be as long as
possible. How about a [link](http://example.org)? We have even created a new line! What a splendid move!
