#!/usr/bin/python

# SPDX-FileCopyrightText: 2021 Nguyen Hung Phu <phuhnguyen@disroot.org>
# SPDX-License-Identifier: LGPL-2.1-or-later

"""
Get names of languages that KDE supports
- Download and extract kconfigwidgets https://download.kde.org/stable/frameworks/
- Update base_lang_dir to point to the po subfolder inside the kconfigwidgets folder
- Run: python lang_names.py
"""

import configparser
import os

import icu
from yaml import dump

from hugoi18n import generation

base_lang_dir = 'kconfigwidgets-5.81.0/po'
manual_names = {
    'crh': 'Къырым Татар',  # https://en.wikipedia.org/wiki/ISO_639:c
    'ha': 'حَوْسَ',  # https://en.wikipedia.org/wiki/ISO_639:h
    'hne': 'छत्तीसगढ़ी',  # https://en.wikipedia.org/wiki/Chhattisgarhi_language
    'zh_HK': '中文（香港）'  # taken from PyICU, but no need to keep all the "SAR of China" shit
}


def get_lang_name(l_code):
    file_path = f'{base_lang_dir}/{l_code}/kf5_entry.desktop'
    with open(file_path) as ini:
        config = configparser.ConfigParser()
        config.read_file(ini)
        section = config['KCM Locale']
        l_key = f'Name[{l_code}]'
        if l_key in section:
            l_name = section[l_key]
        elif l_code in manual_names:
            l_name = manual_names[l_code]
        else:
            locale = icu.Locale(l_code)
            try:
                l_name = locale.getDisplayName(locale)
            except icu.InvalidArgsError:
                print(f'error with language code {l_code}')
    return l_name


if __name__ == "__main__":
    languages = {'en': 'English'}
    for lang_code in os.listdir(base_lang_dir):
        lang_name = get_lang_name(lang_code)
        hugo_lang_code = generation.convert_lang_code(lang_code)
        languages[hugo_lang_code] = lang_name
    with open('hugoi18n/resources/languages.yaml', 'w') as lang_yaml:
        dump(languages, lang_yaml, default_flow_style=False, allow_unicode=True)
