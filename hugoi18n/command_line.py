# SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
# SPDX-FileCopyrightText: 2021 Nguyen Hung Phu <phuhnguyen@disroot.org>
# SPDX-License-Identifier: LGPL-2.1-or-later

import glob
import importlib.resources as pkg_resources
import logging
import os
import subprocess
from argparse import ArgumentParser, RawTextHelpFormatter

from yaml import safe_load

from . import procedure, config, extraction, generation


def main():
    parser = ArgumentParser(description='I18n tool for Hugo projects, working in coordination with scripty')
    parser.add_argument('-q', '--quiet', action='store_true', help='stop showing INFO or lower logs')
    subparsers = parser.add_subparsers(description="used in the process from extracting source files' messages "
                                                   'to generating target files')

    extract_cmd = subparsers.add_parser('extract', help='extract messages from source files',
                                        formatter_class=RawTextHelpFormatter)
    extract_cmd.add_argument('pot', help='either path of the only target pot file or path of the directory\n'
                                         'containing the target pot file(s)')
    extract_cmd.set_defaults(func=extract)

    compile_po_cmd = subparsers.add_parser('compile', help='compile translated messages to binary format',
                                           formatter_class=RawTextHelpFormatter)
    compile_po_cmd.add_argument('dir', help='path of the directory containing either PO files named as {lang}.po, or\n'
                                            'directories with PO files inside {lang}/*.po')
    compile_po_cmd.set_defaults(func=compile_po)

    generate_cmd = subparsers.add_parser('generate', help='generate target messages and files',
                                         formatter_class=RawTextHelpFormatter)
    generate_cmd.set_defaults(func=generate)

    fetch_cmd = subparsers.add_parser('fetch', help='fetch PO files from KDE SVN',
                                      formatter_class=RawTextHelpFormatter)
    fetch_cmd.add_argument('dest', help='destination directory')
    fetch_cmd.add_argument('-f', '--as-file', action='store_true', help='save only the first file in the <lang> '
                                                                        'folder as <lang>.po')
    fetch_cmd.set_defaults(func=fetch)

    args = parser.parse_args()
    level = logging.WARNING if args.quiet else logging.INFO
    logging.basicConfig(format='%(levelname)s: %(message)s', level=level)
    args.func(args)


def extract(args):
    """
    Extract messages from source files
    :param args: arguments passed in command line, containing
        - pot: either path of the only target pot file or path of the directory containing the target pot file(s)
    :return: None
    """
    hi_conf = config.Config()
    dest = args.pot
    proc = procedure.get_proc()
    package = os.environ['PACKAGE']
    if proc == procedure.Proc.PO_FILE:
        pot = extraction.create_and_extract_others(hi_conf, package)
        extraction.extract_content_files(hi_conf.excluded_keys, pot, hi_conf.content['default'], dest)
    else:  # Proc.PO_DIR
        default_pot = extraction.create_and_extract_others(hi_conf, package)
        default_domain_name = procedure.get_default_domain_name()
        os.makedirs(dest, exist_ok=True)
        for domain_name, files in hi_conf.content.items():
            if domain_name == 'default':
                extraction.extract_content_files(hi_conf.excluded_keys, default_pot, files,
                                                 f'{dest}/{default_domain_name}.pot')
            else:
                pot = extraction.create_pot(package)
                extraction.extract_content_files(hi_conf.excluded_keys, pot, files, f'{dest}/{domain_name}.pot')
        default_pot.save(f'{dest}/{default_domain_name}.pot')


def generate(_):
    """
    Generate target messages and files
    :return: None
    """
    en_strings, configs, original_configs = generation.read_sources()

    hi_conf = config.Config()
    with pkg_resources.open_text('hugoi18n.resources', 'languages.yaml') as f_langs:
        lang_names = safe_load(f_langs)
    total_file_cnt = sum([len(x) for _, x in hi_conf.content.items()])
    for lang_code in os.listdir('locale'):
        os.environ["LANGUAGE"] = lang_code
        hugo_lang_code = generation.convert_lang_code(lang_code)
        trans_file_cnt = 0
        default_func = None

        proc = procedure.get_proc()
        if proc == procedure.Proc.PO_FILE:
            domain_name = os.environ['FILENAME']
            default_func = generation.gettext_func(domain_name)

            for file in hi_conf.content['default']:
                if generation.generate_content_file(hi_conf, hugo_lang_code, file, default_func):
                    trans_file_cnt += 1
        else:  # Proc.PO_DIR
            default_domain_name = procedure.get_default_domain_name()
            for domain_name, files in hi_conf.content.items():
                if domain_name == 'default':
                    domain_name = default_domain_name
                mo_path = f'locale/{lang_code}/LC_MESSAGES/{domain_name}.mo'
                if os.path.isfile(mo_path):
                    _ = generation.gettext_func(domain_name)
                    if domain_name == default_domain_name:
                        default_func = _
                    for file in files:
                        if generation.generate_content_file(hi_conf, hugo_lang_code, file, _):
                            trans_file_cnt += 1
            if default_func is None:
                mo_path = f'locale/{lang_code}/LC_MESSAGES/{default_domain_name}.mo'
                if os.path.isfile(mo_path):
                    default_func = generation.gettext_func(default_domain_name)
        logging.info(f'{hugo_lang_code} [{trans_file_cnt}/{total_file_cnt}]')

        if default_func is not None:
            generation.generate_others(hi_conf, lang_names, en_strings, configs, lang_code, hugo_lang_code,
                                       total_file_cnt, trans_file_cnt, default_func)
    generation.write_target(configs, original_configs)


def compile_po(args):
    """
    Compile translated messages to binary format stored in 'locale/{lang}/LC_MESSAGES' directory
    :param args: arguments passed in command line, containing
        - dir: path of the directory containing either:
            - PO files named as {lang}.po, or
            - directories with PO files inside {lang}/*.po
    :return: None
    """
    src_dir = args.dir
    proc = procedure.get_proc()
    if proc == procedure.Proc.PO_FILE:
        domain_name = os.environ['FILENAME']
        for file in os.listdir(src_dir):
            if os.path.splitext(file)[1] == '.pot':
                continue
            lang = os.path.splitext(file)[0]

            target_path = f'locale/{lang}/LC_MESSAGES'
            os.makedirs(target_path, exist_ok=True)
            src = f'{src_dir}/{file}'

            command = f'msgfmt {src} -o {target_path}/{domain_name}.mo'
            subprocess.run(command, shell=True, check=True)
            logging.info(f'Compiled {lang}')
    else:  # Proc.PO_DIR
        for lang in os.listdir(src_dir):
            target_path = f'locale/{lang}/LC_MESSAGES'
            os.makedirs(target_path, exist_ok=True)
            src_path = f'{src_dir}/{lang}'

            for po in os.listdir(src_path):
                po_path = f'{src_path}/{po}'
                mo_path = f'{target_path}/{po[:-2]}mo'
                command = f'msgfmt {po_path} -o {mo_path}'
                subprocess.run(command, shell=True, check=True)
            logging.info(f'Compiled {lang}')


def fetch(args):
    proc = procedure.get_proc()
    if proc == procedure.Proc.PO_FILE and not args.as_file:
        logging.warning('fetch directories while in PO_FILE procedure')
    elif proc == procedure.Proc.PO_DIR and args.as_file:
        logging.warning('fetch files while in PO_DIR procedure')

    svn_pre_path = "svn://anonsvn.kde.org/home/kde/trunk/l10n-kf5"
    svn_sub_path = f"messages/{os.environ['PACKAGE']}"
    po_path = args.dest

    os.makedirs(po_path, exist_ok=True)
    for lang in config.Config().langs:
        svn_path = f"{svn_pre_path}/{lang}/{svn_sub_path}"
        output = f"{po_path}/{lang}"
        try:
            subprocess.check_output(['svn', 'export', svn_path, f'{output}@'], stderr=subprocess.PIPE)
            if args.as_file:
                owd = os.getcwd()
                os.chdir(output)
                fname = f"{os.environ['FILENAME']}.po" if proc == procedure.Proc.PO_FILE else os.listdir()[0]
                os.rename(fname, f'../{lang}.po')
                os.chdir(owd)
                for f in glob.glob(f'{output}/*'):
                    os.remove(f)
                os.rmdir(output)
            logging.info(f"Fetched {lang}")
        except subprocess.CalledProcessError:
            logging.info(f"Non-existent {lang}")
