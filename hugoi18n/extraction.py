# SPDX-FileCopyrightText: 2021 Carl Schwan <carlschwan@kde.org>
# SPDX-FileCopyrightText: 2021 Nguyen Hung Phu <phuhnguyen@disroot.org>
# SPDX-License-Identifier: LGPL-2.1-or-later

import logging
import os

import frontmatter
import polib
from yaml import safe_load

from . import markdown


def append_entry(pot, msgid, occurrences, comment=u''):
    entry = polib.POEntry(
        msgid=msgid,
        msgstr=u'',
        comment=comment,
        occurrences=occurrences
    )
    try:
        pot.append(entry)
    except ValueError:
        pass


def create_pot(package):
    pot = polib.POFile(check_for_duplicates=True)
    pot.metadata = {
        'Project-Id-Version': f'{package} 1.0',
        'Report-Msgid-Bugs-To': 'https://bugs.kde.org',
        'Last-Translator': 'FULL NAME <EMAIL@ADDRESS>',
        'Language-Team': 'LANGUAGE <kde-i18n-doc@kde.org>',
        'MIME-Version': '1.0',
        'Content-Type': 'text/plain; charset=utf-8',
        'Content-Transfer-Encoding': '8bit',
    }
    return pot


def create_and_extract_others(conf, package):
    pot = create_pot(package)

    path = 'config.yaml'
    with open(path, 'r') as f_config:
        configs = safe_load(f_config)
        if conf.do_title:
            if 'title' in configs['languages']['en']:
                append_entry(
                    pot, configs['languages']['en']['title'], [
                        (path, '0')])
        if conf.do_description:
            if 'description' in configs['languages']['en']['params']:
                description = configs['languages']['en']['params']['description']
                append_entry(pot, description, [(path, '0')])
        if conf.do_menu:
            menu = configs['languages']['en']['menu']['main']
            for menu_entry in menu:
                append_entry(pot, menu_entry['name'], [(path, '0')])
    if conf.do_strings:
        path = 'i18n/en.yaml'
        if os.path.isfile(path):
            with open(path, 'r') as f_i18n:
                en_strings = safe_load(f_i18n)
                for _, string in en_strings.items():
                    append_entry(pot, string['other'], [(path, '0')])

    return pot


def extract_frontmatter(excluded_keys, data, pot, filename):
    """
    Extract messages from the frontmatter.

    :param excluded_keys: dict values of these keys are not processed
    :param data: the value currently being processed
    :param pot: the POFile object to extract messages into
    :param filename: tells where the messages are from
    :return: None. Messages are extracted into the POFile object.
    """
    if isinstance(data, str):
        append_entry(pot, data, [(filename, '0')])
    elif isinstance(data, list):
        for item in data:
            extract_frontmatter(excluded_keys, item, pot, filename)
    elif isinstance(data, dict):
        for key in data:
            if key not in excluded_keys:
                extract_frontmatter(excluded_keys, data[key], pot, filename)


def add_entry_to_pot(pot, entry, filename, line_number, comment=u''):
    if entry != "":
        old_entry = pot.find(entry)
        if old_entry:
            old_entry.occurrences.append((filename, line_number))
        else:
            append_entry(pot, entry, [(filename, line_number)], comment)


def fm_length(fm):
    """
    Approximate number of lines a frontmatter takes up, using number of fields it has

    :param fm: the metadata field of a frontmatter.Post object
    :return: approximated number of lines the frontmatter takes up
    """
    if not isinstance(fm, (list, dict)):
        return 1
    elif isinstance(fm, list):
        return sum([fm_length(f) for f in fm])
    else:  # dict
        return sum([(1 if not isinstance(v, (list, dict))
                   else 1 + fm_length(v)) for v in fm.values()])


def extract_paragraph(paragraph, pot, filename, line_number):
    """
    Extract messages from a paragraph, add them to the `POFile` object

    :param paragraph: the paragraph, messages from which are to be extracted
    :param pot: the POFile object to extract messages into
    :param filename: tells where the messages are from
    :param line_number: line number where the paragraph is in the file
    :return: None. Messages are extracted into the POFile object.
    """
    if paragraph:
        extracted_parts = markdown.parse_paragraph(paragraph)
        for message in extracted_parts[0]:
            add_entry_to_pot(pot, message, filename, line_number)


def extract_code_block(code_block, pot, filename, line_number):
    if code_block:
        add_entry_to_pot(pot, code_block[3:], filename, line_number)


def extract_content(content, pot, filename: str):
    """
    Extract messages from page content

    :param content: the content field of a frontmatter.Post object
    :param pot: the POFile object to extract messages into
    :param filename: tells where the messages are from
    :return: None. Messages are extracted into the POFile object.
    """
    paragraph = ""
    line_number = 4  # approximation
    in_code_block = False

    for line in content.splitlines():
        line = ' '.join(line.split())
        do_paragraph, do_line, _, stop, enter_code_block, next_paragraph = markdown.parse_line(
            line, paragraph, '', '', in_code_block)
        if do_paragraph:
            if in_code_block:
                extract_code_block(paragraph, pot, filename, line_number)
            else:
                extract_paragraph(paragraph, pot, filename, line_number)
        if enter_code_block:
            in_code_block = not in_code_block
        if do_line:
            extract_paragraph(line, pot, filename, line_number + 1)
        paragraph = next_paragraph
        if stop:
            break
        line_number += 1
    # when the last line still contains content
    extract_paragraph(paragraph, pot, filename, line_number)


def extract_content_files(excluded_keys, pot, files, dest):
    for f in files:
        logging.info(f)
        page = frontmatter.load(f)
        extract_frontmatter(excluded_keys, page.metadata, pot, f)
        extract_content(page.content, pot, f)
    pot.save(dest)
