# SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
# SPDX-FileCopyrightText: 2021 Nguyen Hung Phu <phuhnguyen@disroot.org>
# SPDX-License-Identifier: LGPL-2.1-or-later

import re
import typing as tp

patterns = {
    'list_item': re.compile('( *(?:[*+-]|[0-9]+[.)]) +)(.*)'),
    'setext_heading': re.compile('=+|-+$'),
    'atx_heading': re.compile('( *#{1,6} +)(.*)'),
    'blockquote': re.compile('(> {0,4})(.*)'),
    'i18n_var': re.compile(r'({{[<%] ?i18n_var )([`"])(.+?)(?<!\\)\2'),
    'shortcode_arg': re.compile(r'(caption|description|alt|title)="(.+?)(?<!\\)"'),
    'shortcode': re.compile('{{[<%]'),
    'code_block': re.compile('^```'),
    'comment': re.compile('([ \t]*// )(.*)'),
}


def escape_braces(string):
    return string.replace('{', '{{').replace('}', '}}')


def parse_parts(subparagraph: str):
    if findings := patterns['i18n_var'].findall(subparagraph):
        return [f[2] for f in findings], patterns['i18n_var'].sub(r'\1`{}`', escape_braces(subparagraph))
    if patterns['shortcode'].match(subparagraph):
        if findings := patterns['shortcode_arg'].findall(subparagraph):
            return [f[1] for f in findings], patterns['shortcode_arg'].sub(r'\1=`{}`', escape_braces(subparagraph))
        return [], escape_braces(subparagraph)
    return [subparagraph], '{}'


def parse_paragraph(paragraph: str, prefix='') -> tp.Tuple[tp.List[str], str]:
    """
    Parse a paragraph whose lines have been joined into a single line, extract messages out of it, parts that are left
    are made into a format string to recreate the paragraph.

    :param paragraph: the paragraph that has been joined to a line
    :param prefix: the part at the beginning of the paragraph, to mark a blockquote
    :return: a tuple containing a list of extracted messages, and a format string of the original paragraph
    """
    paragraph = paragraph.rstrip()
    if (match := patterns['atx_heading'].match(paragraph)) or (match := patterns['list_item'].match(paragraph)):
        subparagraph = match.group(2)
        parts = parse_parts(subparagraph)
        return parts[0], f'{prefix}{match.group(1)}{parts[1]}'

    if (match := patterns['blockquote'].match(paragraph)) or (match := patterns['comment'].match(paragraph)):
        return parse_paragraph(match.group(2), f'{prefix}{match.group(1)}')

    parts = parse_parts(paragraph)
    return parts[0], f'{prefix}{parts[1]}'


def parse_line(line, paragraph, line_prefix='', par_prefix='', in_code_block=False):
    """
    Parse one line of text, join it into ``paragraph`` or process ``paragraph``, depending on the line content.

    :param line: the line to process
    :param paragraph: the paragraph currently being processed
    :param line_prefix: the part at the beginning of the line, to mark a blockquote
    :param par_prefix: the part at the beginning of the paragraph, to mark a blockquote
    :return: a tuple containing flags of whether to process the paragraph, whether to process the line right away,
    whether the line contains no content, and whether to stop extraction at this line, and the updated value of the
    paragraph (reset after being processed, or joined by the line)
    """
    do_paragraph, do_line, non_content, stop, enter_code_block, next_paragraph = False, False, False, False, False, ''

    if 'stop-translate' in line:
        do_paragraph = True
        stop = True
    elif patterns['atx_heading'].match(line):
        do_paragraph = True
        do_line = True
    elif in_code_block:
        if line_match := patterns['comment'].match(line):
            pre_line, sub_line = f'{line_prefix}{line_match.group(1)}', line_match.group(2)
            if paragraph_match := patterns['comment'].match(paragraph):
                pre_par, sub_par = f'{par_prefix}{paragraph_match.group(1)}', paragraph_match.group(2)
                do_paragraph, do_line, non_content, stop, enter_code_block, next_paragraph = \
                    parse_line(sub_line, sub_par, pre_line, pre_par)
            elif not paragraph:
                do_paragraph = False
                next_paragraph = pre_line + sub_line

        elif patterns['code_block'].match(line):
            enter_code_block = True
            next_paragraph = ""
            do_paragraph = True
            non_content = True
        else:
            non_content = True
            do_paragraph = True

    elif patterns['code_block'].match(line):
        enter_code_block = True
        next_paragraph = ""
        non_content = True
        do_paragraph = True
    elif not line or patterns['setext_heading'].match(line):
        do_paragraph = True
        non_content = True
    elif patterns['list_item'].match(line):
        do_paragraph = True
        next_paragraph = f'{line_prefix}{line}'
    elif line_match := patterns['blockquote'].match(line):
        pre_line, sub_line = f'{line_prefix}{line_match.group(1)}', line_match.group(2)
        if paragraph_match := patterns['blockquote'].match(paragraph):
            pre_par, sub_par = f'{par_prefix}{paragraph_match.group(1)}', paragraph_match.group(2)
            do_paragraph, do_line, non_content, stop, enter_code_block, next_paragraph = \
                parse_line(sub_line, sub_par, pre_line, pre_par)
        else:
            do_paragraph = True
            _, do_line, non_content, stop, enter_code_block, next_paragraph = parse_line(sub_line, '', pre_line)
    else:
        next_paragraph = f'{line_prefix}{line}' if not paragraph else f'{par_prefix}{paragraph} {line}'
    return do_paragraph, do_line, non_content, stop, enter_code_block, next_paragraph
