# SPDX-FileCopyrightText: 2021 Carl Schwan <carlschwan@kde.org>
# SPDX-FileCopyrightText: 2021 Nguyen Hung Phu <phuhnguyen@disroot.org>
# SPDX-License-Identifier: LGPL-2.1-or-later

import copy
import gettext
import logging
import os
import typing as tp
import textwrap

import frontmatter
from yaml import safe_load, dump

from . import config, markdown

trans_table = str.maketrans('@_', '--')
lang_code_at_dict = {
    'cy': 'cyrillic',
    'ije': 'ijekavian',
    'il': 'ijekavianlatin',
    'la': 'latin',
    'va': 'valencia'
}


def convert_lang_code(lang_code):
    if lang_code == 'pt':
        return 'pt-pt'
    else:
        hugo_lang_code = lang_code.lower().translate(trans_table)
        parts = hugo_lang_code.split('-')
        if len(parts) > 1:
            if parts[1] == 'ijekavian':
                hugo_lang_code = f'{parts[0]}-ije'
            elif parts[1] == 'ijekavianlatin':
                hugo_lang_code = f'{parts[0]}-il'
            elif len(parts[1]) > 2:
                hugo_lang_code = f'{parts[0]}-{parts[1][0:2]}'
        return hugo_lang_code


def revert_lang_code(hugo_lang_code):
    if hugo_lang_code == 'pt-pt':
        return 'pt'
    else:
        parts = hugo_lang_code.split('-')
        if len(parts) > 1:
            if parts[1] in lang_code_at_dict.keys():
                return f'{parts[0]}@{lang_code_at_dict[parts[1]]}'
            else:
                return f'{parts[0]}_{parts[1].upper()}'
        else:
            return hugo_lang_code


def generate_frontmatter(excluded_keys, data, _):
    """
    Generate translation of the frontmatter.

    :param excluded_keys: dict values of these keys are not processed
    :param data: the value currently being processed
    :param _: a gettext.gettext function
    :return: a boolean of whether there has been at least one translation made. Translations are generated in-place.
    """
    has_translations = False
    if isinstance(data, list):
        for index, item in enumerate(data):
            if isinstance(item, str):
                # in frontmatter only count translations that are different
                # from original messages
                if item != _(item):
                    has_translations = True
                    data[index] = _(item)
            else:
                has_translations = generate_frontmatter(
                    excluded_keys, item, _) or has_translations
    elif isinstance(data, dict):
        for key, value in data.items():
            if key in excluded_keys:
                continue
            if not value:
                data[key] = ''
            elif isinstance(value, str):
                if value != _(value):
                    has_translations = True
                    data[key] = _(value)
            else:
                has_translations = generate_frontmatter(
                    excluded_keys, value, _) or has_translations

    return has_translations


def generate_paragraph(paragraph, total_msg_cnt, trans_msg_cnt, _):
    """
    Generate translation of a paragraph, update accumulated total number of messages and number of translated messages

    :param paragraph: the paragraph, translation of which is to be generated
    :param total_msg_cnt: accumulated total number of messages in the page until this paragraph
    :param trans_msg_cnt: accumulated number of translated messages in the page until this paragraph
    :param _: the gettext.gettext function
    :return: a tuple containing the translation of the paragraph, updated total number of messages and updated number
        of translated messages
    """
    if paragraph:
        extracted_parts = markdown.parse_paragraph(paragraph)
        if extracted_parts[0]:
            trans_msgs = []
            for message in extracted_parts[0]:
                tr_msg = _(message)
                trans_msgs.append(tr_msg)
                if tr_msg is not message:
                    trans_msg_cnt += 1
                total_msg_cnt += 1
            paragraph = extracted_parts[1].format(*trans_msgs)
        paragraph += '\n'
    return paragraph, total_msg_cnt, trans_msg_cnt


def generate_codeblock(paragraph, total_msg_cnt, trans_msg_cnt, _):
    """
    Generate translation of a paragraph inside a code block, update accumulated total number of messages and
    number of translated messages. The difference with generate_paragraph is that comments are wrapped.

    :param paragraph: the paragraph, translation of which is to be generated
    :param total_msg_cnt: accumulated total number of messages in the page until this paragraph
    :param trans_msg_cnt: accumulated number of translated messages in the page until this paragraph
    :param _: the gettext.gettext function
    :return: a tuple containing the translation of the paragraph, updated total number of messages and updated number
        of translated messages
    """
    if paragraph:
        extracted_parts = markdown.parse_paragraph(paragraph)
        if extracted_parts[0]:
            trans_msgs = []
            for message in extracted_parts[0]:
                tr_msg = _(message)
                trans_msgs.append(tr_msg)
                if tr_msg is not message:
                    trans_msg_cnt += 1
                total_msg_cnt += 1
            wrap_at_length = 100 - len(extracted_parts[1])
            paragraph = ""
            lines = textwrap.wrap(trans_msgs[0], wrap_at_length)
            for i in range(len(lines)):
                paragraph += extracted_parts[1].format(
                    lines[i]) + ("\n" if i < len(lines) - 1 else "")
        paragraph += '\n'
    return paragraph, total_msg_cnt, trans_msg_cnt


def generate_content(content, _) -> tp.Tuple[str, float]:
    """
    Generate translated content of a page

    :param content: the content field of a frontmatter.Post object
    :param _: a gettext.gettext function
    :return: a tuple containing a string of the generated content and a float of percentage of number of translated
        messages over total number of messages in the content.
    """
    paragraph = ""
    gen_content = ""
    trans_msg_cnt = 0
    total_msg_cnt = 0
    in_code_block = False

    for line in content.splitlines():
        if not in_code_block:
            line = ' '.join(line.split())
        do_paragraph, do_line, non_content, stop, enter_code_block, next_paragraph = markdown.parse_line(
            line, paragraph, '', '', in_code_block)
        if do_paragraph:
            if in_code_block:
                gen_paragraph, total_msg_cnt, trans_msg_cnt = generate_codeblock(
                    paragraph, total_msg_cnt, trans_msg_cnt, _)
                gen_content += gen_paragraph + \
                    (f'{line}\n' if non_content else '')
            else:
                gen_paragraph, total_msg_cnt, trans_msg_cnt = generate_paragraph(
                    paragraph, total_msg_cnt, trans_msg_cnt, _)
                gen_content += gen_paragraph + \
                    (f'{line}\n' if non_content else '')
        if enter_code_block:
            in_code_block = not in_code_block
        if do_line:
            gen_paragraph, total_msg_cnt, trans_msg_cnt = generate_paragraph(
                line, total_msg_cnt, trans_msg_cnt, _)
            gen_content += gen_paragraph
        paragraph = next_paragraph
        if stop:
            break
    # when the last line still contains content
    gen_paragraph, total_msg_cnt, trans_msg_cnt = generate_paragraph(
        paragraph, total_msg_cnt, trans_msg_cnt, _)
    gen_content += gen_paragraph

    return gen_content, trans_msg_cnt / total_msg_cnt if total_msg_cnt > 0 else -1


def generate_content_file(hi_conf, hugo_lang_code, file: str, _):
    if hi_conf.gen_to_other_dir:
        target_file = file.replace(
            'content/', f'content-trans/{hugo_lang_code}/')
    else:
        extension = os.path.splitext(file)[1]
        basename = os.path.splitext(file)[0].split('.')[0]
        target_file = f'{basename}.{hugo_lang_code}{extension}'
    os.makedirs(os.path.dirname(target_file), exist_ok=True)

    page = frontmatter.load(file)
    has_translations = generate_frontmatter(
        hi_conf.excluded_keys, page.metadata, _)
    gen_content, percent = generate_content(page.content, _)
    if percent > 0.5 or percent == -1 or has_translations:
        with open(target_file, 'w+') as f_target:
            f_target.write('---\n')
            f_target.write(
                dump(
                    page.metadata,
                    default_flow_style=False,
                    allow_unicode=True))
            f_target.write('---\n')
            f_target.write(gen_content)
        return True
    return False


def generate_strings(en_strings, hugo_lang_code, _):
    target_strings = {}
    for key, string in en_strings.items():
        if _(string['other']) is not string['other']:
            target_strings[key] = {}
            target_strings[key]['other'] = _(string['other'])
    if len(target_strings) > 0:
        i18n_path = f'i18n/{hugo_lang_code}.yaml'
        with open(i18n_path, 'w+') as f_target_i18n:
            logging.info(i18n_path)
            f_target_i18n.write(
                dump(
                    target_strings,
                    default_flow_style=False,
                    allow_unicode=True))
        return True
    return False


def generate_languages(configs, lang_names, lang_code, hugo_lang_code, _):
    if hugo_lang_code not in configs['languages']:
        configs['languages'][hugo_lang_code] = {}
    configs['languages'][hugo_lang_code]['languageCode'] = lang_code
    configs['languages'][hugo_lang_code]['weight'] = 2

    if lang_code in config.all_lang_codes or 'languageName' not in configs[
            'languages'][hugo_lang_code]:
        configs['languages'][hugo_lang_code]['languageName'] = lang_names[hugo_lang_code]


def generate_menu(configs, hugo_lang_code, _):
    configs['languages'][hugo_lang_code]['menu'] = {}
    configs['languages'][hugo_lang_code]['menu']['main'] = []
    for menu_item in configs['languages']['en']['menu']['main']:
        target_menu_item = copy.deepcopy(menu_item)
        target_menu_item['name'] = _(target_menu_item['name'])
        configs['languages'][hugo_lang_code]['menu']['main'].append(
            target_menu_item)


def generate_description(configs, hugo_lang_code, _):
    configs['languages'][hugo_lang_code]['params'] = {}
    configs['languages'][hugo_lang_code]['params']['description'] = \
        _(configs['languages']['en']['params']['description'])


def generate_title(configs, hugo_lang_code, _):
    configs['languages'][hugo_lang_code]['title'] = _(
        configs['languages']['en']['title'])


def generate_others(
        hi_conf,
        lang_names,
        en_strings,
        configs,
        lang_code,
        hugo_lang_code,
        total_file_cnt,
        trans_file_cnt,
        _):
    """
    Generate i18n files and config fields
    """
    strings_count_ok = True
    if hi_conf.do_strings and en_strings is not None:
        strings_count_ok = generate_strings(en_strings, hugo_lang_code, _)

    # Only generate the config section for the language if conditions are met
    if not strings_count_ok or (
            total_file_cnt > 0 and trans_file_cnt <= 0) or 'languages' not in configs:
        return

    generate_languages(configs, lang_names, lang_code, hugo_lang_code, _)
    if hi_conf.gen_to_other_dir:
        configs['languages'][hugo_lang_code][
            'contentDir'] = f'content-trans/{hugo_lang_code}'
    if hi_conf.do_menu:
        generate_menu(configs, hugo_lang_code, _)
    if hi_conf.do_description:
        generate_description(configs, hugo_lang_code, _)
    if hi_conf.do_title:
        generate_title(configs, hugo_lang_code, _)


def gettext_func(domain_name):
    gettext.bindtextdomain(domain_name, 'locale')
    gettext.textdomain(domain_name)
    return gettext.gettext


def read_sources():
    try:
        with open("i18n/en.yaml", 'r') as f_i18n:
            en_strings = safe_load(f_i18n)
    except FileNotFoundError:
        en_strings = None
    with open("config.yaml", 'r') as f_config:
        configs = safe_load(f_config)
        original_configs = copy.deepcopy(configs)
    return en_strings, configs, original_configs


def write_target(configs, original_configs):
    if configs != original_configs:
        with open('config.yaml', 'w+') as f_config:
            f_config.write(
                dump(
                    configs,
                    default_flow_style=False,
                    allow_unicode=True))
