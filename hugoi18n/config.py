# SPDX-FileCopyrightText: 2021 Carl Schwan <carlschwan@kde.org>
# SPDX-FileCopyrightText: 2021 Nguyen Hung Phu <phuhnguyen@disroot.org>
# SPDX-License-Identifier: LGPL-2.1-or-later

import glob

from yaml import safe_load

from .procedure import Proc, get_proc


def read_domain_config(domain_config):
    """
    Get files of a domain by reading the config
    :param domain_config: config of the domain to read
    :return: list of files of the domain
    """
    if 'files' not in domain_config and 'globs' not in domain_config:
        raise ValueError("domain config contains neither 'files' nor 'globs' field")
    domain_files = []
    if 'files' in domain_config:
        domain_files += domain_config['files']
    if 'globs' in domain_config:
        for g in domain_config['globs']:
            domain_files += glob.glob(g)
    return domain_files


def read_content_extraction_config(i18n_config):
    """
    Retrieve lists of files to extract content, grouped by domains
    :param i18n_config: the i18n config section
    :return: a dict with domain names as keys and file lists as values
    """
    if 'content' not in i18n_config:
        return {'default': []}

    c_config = i18n_config['content']
    content_to_extract = {}
    proc = get_proc()
    if proc == Proc.PO_FILE:
        content_to_extract['default'] = [] if 'default' not in c_config else read_domain_config(c_config['default'])
    else:  # Proc.PO_DIR
        for domain in c_config:
            content_to_extract[domain] = read_domain_config(c_config[domain])
    return content_to_extract


all_lang_codes = "af ar as ast az be be@latin bg bn bn_IN br bs ca ca@valencia crh cs csb cy da de el en en_GB eo es " \
                 "et eu fa fi fr fy ga gd gl gu ha he hl hne hr hsb hu hy ia id is it ja ka kk km kn ko ku lb lt lv " \
                 "mai mk ml mr ms mt nb nds ne nl nn nso oc or pa pl ps pt pt_BR ro ru rw se si sk sl sq sr " \
                 "sr@ijekavian sr@ijekavianlatin sr@latin sv ta te tg th tn tr tt ug uk uz uz@cyrillic vi wa xh " \
                 "zh_CN zh_HK zh_TW"
excluded_keys = ['aliases', 'authors', 'date', 'jsFiles', 'layout', 'parent', 'publishDate', 'sassFiles',
                 'SPDX-License-Identifier', 'type']


class Config:
    def __init__(self):
        with open('config.yaml', 'r') as stream:
            configs = safe_load(stream)
            if 'i18n' not in configs:
                return

            i18n_config = configs['i18n']
            self.gen_to_other_dir = i18n_config['genToOtherDir'] if 'genToOtherDir' in i18n_config else False
            self.do_title = 'others' in i18n_config and 'title' in i18n_config['others']
            self.do_description = 'others' in i18n_config and 'description' in i18n_config['others']
            self.do_menu = 'others' in i18n_config and 'menu' in i18n_config['others']
            self.do_strings = 'others' in i18n_config and 'strings' in i18n_config['others']
            self.content = read_content_extraction_config(i18n_config)
            self.excluded_keys = excluded_keys + (
                i18n_config['excludedKeys'].split() if 'excludedKeys' in i18n_config else [])
            self.langs = (i18n_config['langs'] if 'langs' in i18n_config else all_lang_codes).split()
