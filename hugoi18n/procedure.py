# SPDX-FileCopyrightText: 2021 Nguyen Hung Phu <phuhnguyen@disroot.org>
# SPDX-License-Identifier: LGPL-2.1-or-later

import os
from enum import Enum


class Proc(Enum):
    PO_FILE = 0
    PO_DIR = 1


def get_proc():
    """
    Get type of procedure based on environment variables set by scripty
    :return: a Proc object
    """
    if 'FILENAME' in os.environ and os.environ['FILENAME']:
        return Proc.PO_FILE
    else:
        return Proc.PO_DIR


def get_default_domain_name():
    """
    Get the default domain name in PO_DIR procedure.
    At compilation and generation steps, PACKAGE variable needs to be set
    :return: name of the default domain
    """
    package = os.environ['PACKAGE']
    if package == 'websites-kde-org':
        return 'menu_shared'
    elif package == 'websites-timeline-kde-org':
        return 'timeline-kde-org-shared'
    else:
        return '-'.join(package.split('-')[1:])
