<!--
SPDX-FileCopyrightText: 2021 Carl Schwan <carlschwan@kde.org>
SPDX-FileCopyrightText: 2021 Nguyen Hung Phu <phuhnguyen@disroot.org>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# I18n tool for Hugo projects

A Python package to support the translation of [Hugo](https://gohugo.io/) projects with [gettext](https://www.gnu.org/software/gettext/).

## Description

Following terminologies and workflows of [KDE's Scripty](https://invent.kde.org/sysadmin/l10n-scripty), `hugo-i18n` defines two procedures for working with PO files in the process of translating a Hugo website: working with one PO file and working with a directory of PO file(s).

In each procedure, there are three main steps: _extraction_ (to produce POT files from source files in English), _compilation_ (to make binary files from PO files), and _generation_ (to generate files in target languages using binary files from _compilation_ step). A _fetch_ step (to download PO files from [KDE SVN](https://websvn.kde.org/) into the required structure) is also included for development.

The `FILENAME` environment variable is used to determine which procedure is in operation, and together with the `PACKAGE` environment variable, to name fields and files. `$PACKAGE` contains the KDE project ID of the Hugo project that `hugo-i18n` is working on. For example, for the project of the [kate-editor.org](https://kate-editor.org/) website, `$PACKAGE` is "websites-kate-editor-org".

In the table below:
- `"<domain>" content messages` stands for messages from content files listed in the `[i18n.content.<domain>]` config block (see the [configuration](#all-effective-configuration) section below);
- `other messages` stands for strings from `i18n/en.yaml` and site title, site description, and menu entry names from `config.yaml`;
- `<default domain>` stands for the value of `$PACKAGE` after removing the "websites-" part at the beginning  
  Exceptions due to historical reason:
  - [kde.org](https://kde.org): `$PACKAGE` is "websites-kde-org", `<default domain>` is "menu_shared";
  - [timeline.kde.org](https://timeline.kde.org): `$PACKAGE` is "websites-timeline-kde-org", `<default domain>` is "timeline-kde-org-shared"

| Procedure<br>\ <br>Step | PO_FILE<br>`$FILENAME` is non-empty | PO_DIR<br>`$FILENAME` is not non-empty |
|---|---|---|
| Extraction | - Input: Path of a POT file<br>- Extracts `"default" content messages` and `other messages` into that file. | - Input: Path of a directory containing POT file(s) to be produced, the number of them are determined by configs in `config.yaml`<br>- Extracts `other messages` and `"default" content messages` (if the `[i18n.content.default]` config block exists) into `<default domain>.pot`<br>- Extracts `"<X>" content messages` ("\<X\>" is not "default") into `<X>.pot` |
| Fetch | - Input: Path of a directory<br>- Downloads PO files into the directory, file names are changed to language codes `<lang>.po`<br>- The `fetch` subcommand does this with `-f` argument | - Input: Path of a directory<br>- Downloads PO files into subfolders of the directory, one folder for each language, file names are not changed `<lang>/<X>.po`<br>- The `fetch` subcommand does this by default |
| Compilation | - Input: Same as input of `fetch` step<br>- Compiles each PO file `<lang>.po` to a MO file `$FILENAME.mo` in `locale/<lang>/LC_MESSAGES` | - Input: Same as input of `fetch` step<br>- Compiles each PO file `<lang>/<X>.po` to a MO file `<X>.mo` in `locale/<lang>/LC_MESSAGES` |
| Generation | Generates translations for `other messages` and `"default" content messages` using `$FILENAME.mo` in `locale/<lang>/LC_MESSAGES` | Generates translations for `other messages` and all `"<X>" content messages` using `<X>.mo` files in `locale/<lang>/LC_MESSAGES` |

## All effective configuration

`hugo-i18n` uses Hugo's `config.yaml` to store its configs:

```yaml
i18n: # required to use hugo-i18n
  content: # hugo-i18n will work on content files
    default: # required for PO_FILE procedure, optional for PO_DIR procedure
      files: # single files
      - content/a_name_only_for_demonstration.md
      - content/another_one.md
      globs: # multiple files with similar paths/names
      - content/some_folder/*.md
      - content/files_numbered_from_1_to_100/2*.md
    # there must be at least "globs" or "files" key in a domain listing dict
    # only 'default' is a fixed key name, names for other keys of the 'content' dict are by choice
    domain_only_globs:
      globs:
      - content/announcements/13/13.1[1-3].0/*
      - content/announcements/13/13.11.*.md
      - content/announcements/cog/*.md
    domain_only_files:
      files:
      - content/_index.md
  # hugo-i18n will work on strings in i18n/ folder and site title, site description, and menu entry names in config.yaml
  others: [strings, menu, title, description]
  # whether to leave "content" directory only for English files
  # by default, this is false, hugo-i18n will generate target files in the same location as English ones, only change the language code in file names
  # if true, hugo-i18n will generate target files in "content-trans" directory
  genToOtherDir: true
  # a space-separated string of frontmatter keys to exclude from extraction and generation
  # this custom key list will be joined with the default key list in config.py to make the full list of excluded frontmatter keys
  excluded_keys: ''
  # a space-separated string of language codes, only PO files of these languages will be fetched
  # only used for projects in PO_DIR procedure, whose fetch step is not handled by Scripty but by the build script
  # the all_lang_codes variable in config.py contains all possible language codes
  langs: 'ast ca ca@valencia cs es et eu fi fr gl hu it ko nl pt pt_BR ru sk sv tr uk'
```

## Usage

- The main command is `hugoi18n`
- 4 subcommands are verbs corresponding to name of the 4 steps, the input of each subcommand is also described in the table above
- Remember to set environment variables before running commands.

## License

This project is licensed under LGPL-2.1-or-later.
